﻿using System;

namespace AuthManager.Domain.Models.Data
{
    public partial class PermissionRole
    {
        public Guid Id { get; set; }
        public string Label { get; set; }
        public Guid PermissionId { get; set; }
        public Guid RoleId { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }

        public virtual Permission Permission { get; set; }
        public virtual Role Role { get; set; }
    }
}
