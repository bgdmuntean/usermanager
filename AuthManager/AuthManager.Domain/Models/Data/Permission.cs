﻿using System;
using System.Collections.Generic;

namespace AuthManager.Domain.Models.Data
{
    public partial class Permission
    {
        public Permission()
        {
            PermissionClaims = new HashSet<PermissionClaim>();
            PermissionRoles = new HashSet<PermissionRole>();
        }

        public Guid Id { get; set; }
        public string Label { get; set; }
        public Guid EntityId { get; set; }
        public Guid OperationId { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }

        public virtual Entity Entity { get; set; }
        public virtual Operation Operation { get; set; }
        public virtual ICollection<PermissionClaim> PermissionClaims { get; set; }
        public virtual ICollection<PermissionRole> PermissionRoles { get; set; }
    }
}
