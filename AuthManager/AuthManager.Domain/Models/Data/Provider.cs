﻿using System;
using System.Collections.Generic;

namespace AuthManager.Domain.Models.Data
{
    public partial class Provider
    {
        public Provider()
        {
            Logins = new HashSet<Login>();
            Tokens = new HashSet<Token>();
        }

        public Guid Id { get; set; }
        public string Label { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }

        public virtual ICollection<Login> Logins { get; set; }
        public virtual ICollection<Token> Tokens { get; set; }
    }
}
