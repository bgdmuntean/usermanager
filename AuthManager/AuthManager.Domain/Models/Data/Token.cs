﻿using System;

namespace AuthManager.Domain.Models.Data
{
    public partial class Token
    {
        public Guid Id { get; set; }
        public Guid ProviderId { get; set; }
        public string Value { get; set; }
        public string RefreshToken { get; set; }
        public bool IsActive { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }
        public DateTimeOffset ExpiresAt { get; set; }

        public virtual Provider Provider { get; set; }
    }
}
