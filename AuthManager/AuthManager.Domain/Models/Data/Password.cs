﻿using System;

namespace AuthManager.Domain.Models.Data
{
    public partial class Password
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string Hash { get; set; }
        public bool IsActive { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }

        public virtual User User { get; set; }
    }
}
