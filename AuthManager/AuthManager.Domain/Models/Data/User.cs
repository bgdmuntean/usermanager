﻿using System;
using System.Collections.Generic;

namespace AuthManager.Domain.Models.Data
{
    public partial class User
    {
        public User()
        {
            ClaimUsers = new HashSet<ClaimUser>();
            Logins = new HashSet<Login>();
            Passwords = new HashSet<Password>();
        }

        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool Confirmed { get; set; }
        public string Phone { get; set; }
        public int AccessFailedCount { get; set; }
        public bool SoMeUser { get; set; }
        public Guid? RoleId { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }

        public virtual Role Role { get; set; }
        public virtual ICollection<ClaimUser> ClaimUsers { get; set; }
        public virtual ICollection<Login> Logins { get; set; }
        public virtual ICollection<Password> Passwords { get; set; }
    }
}
