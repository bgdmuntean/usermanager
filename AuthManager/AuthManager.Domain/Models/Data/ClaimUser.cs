﻿using System;

namespace AuthManager.Domain.Models.Data
{
    public partial class ClaimUser
    {
        public Guid Id { get; set; }
        public Guid ClaimId { get; set; }
        public Guid UserId { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }

        public virtual Claim Claim { get; set; }
        public virtual User User { get; set; }
    }
}
