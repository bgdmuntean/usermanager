﻿using System;
using System.Collections.Generic;

namespace AuthManager.Domain.Models.Data
{
    public partial class Claim
    {
        public Claim()
        {
            ClaimUsers = new HashSet<ClaimUser>();
            PermissionClaims = new HashSet<PermissionClaim>();
        }

        public Guid Id { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }

        public virtual ICollection<ClaimUser> ClaimUsers { get; set; }
        public virtual ICollection<PermissionClaim> PermissionClaims { get; set; }
    }
}
