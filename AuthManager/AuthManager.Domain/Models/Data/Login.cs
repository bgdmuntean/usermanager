﻿using System;

namespace AuthManager.Domain.Models.Data
{
    public partial class Login
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid ProviderId { get; set; }
        public string Hash { get; set; }
        public bool IsActive { get; set; }
        public DateTimeOffset CreatedAt { get; set; }

        public virtual Provider Provider { get; set; }
        public virtual User User { get; set; }
    }
}
