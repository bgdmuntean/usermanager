﻿using System;

namespace AuthManager.Domain.Models.Data
{
    public partial class PermissionClaim
    {
        public Guid Id { get; set; }
        public string Label { get; set; }
        public Guid PermissionId { get; set; }
        public Guid ClaimId { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }

        public virtual Claim Claim { get; set; }
        public virtual Permission Permission { get; set; }
    }
}
