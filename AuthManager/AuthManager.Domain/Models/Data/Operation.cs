﻿using System;
using System.Collections.Generic;

namespace AuthManager.Domain.Models.Data
{
    public partial class Operation
    {
        public Operation()
        {
            Permissions = new HashSet<Permission>();
        }

        public Guid Id { get; set; }
        public string Label { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }

        public virtual ICollection<Permission> Permissions { get; set; }
    }
}
